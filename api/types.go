package api

const (
	LEFT  Direction = "left"
	RIGHT Direction = "right"
	UP    Direction = "up"
	DOWN  Direction = "down"
)

//BattleSnakeInfoResponse is the type that is sent back to the battlesnake server to make sure the snek is ok.
type BattleSnakeInfoResponse struct {
	APIVersion string `json:"apiversion"`
	Author     string `json:"author"`
	Color      string `json:"color"`
	Head       string `json:"head"`
	Tail       string `json:"tail"`
}

//Game is the object that gets sent at the start of the game and each turn to
type BattleSnakeGame struct {
	Id      string             `json:"id"`
	Ruleset BattleSnakeRuleset `json:"ruleset"`
	Timeout int                `json:"timeout"`
	Source  string             `json:"source"`
}

//Settings represents the settings for the given game
type BattleSnakeSettings struct {
	FoodSpawnChance     int                       `json:"foodSpawnChance"`
	MinimumFood         int                       `json:"minimumFood"`
	HazardDamagePerTurn int                       `json:"hazardDamagePerTurn"`
	RoyaleSettings      BattleSnakeRoyaleSettings `json:"royale"`
	SquadSettings       BattleSnakeSquadSettings  `json:"squad"`
}

//RoyaleSettings are the settings that are only applicable in Royale game modes
type BattleSnakeRoyaleSettings struct {
	ShrinkEveryNTurns int `json:"shrinkEveryNTurns"`
}

//SquadSettings are the settings that are only applicable in Squad game modes
type BattleSnakeSquadSettings struct {
	AllowBodyCollisions bool `json:"allowBodyCollisions"`
	SharedElimination   bool `json:"sharedEliminations"`
	SharedHealth        bool `json:"sharedHealth"`
	SharedLength        bool `json:"sharedLength"`
}

//Ruleset represents the rules for the given game
type BattleSnakeRuleset struct {
	Name     string              `json:"name"`
	Version  string              `json:"version"`
	Settings BattleSnakeSettings `json:"settings"`
}

//Board is the object that gets sent at the start of the game and each turn to represent the board state
type BattleSnakeBoard struct {
	Height  int                `json:"height"`
	Width   int                `json:"width"`
	Food    []Coordinates      `json:"food"`
	Hazards []Coordinates      `json:"hazards"`
	Snakes  []BattleSnakeSnake `json:"snakes"`
}

//Snake is the object that gets sent at the start of the game and each turn to represent my snake
type BattleSnakeSnake struct {
	Id      string        `json:"id"`
	Name    string        `json:"name"`
	Health  int           `json:"health"`
	Body    []Coordinates `json:"body"`
	Latency string        `json:"latency"`
	Head    Coordinates   `json:"head"`
	Length  int           `json:"length"`
	Shout   string        `json:"shout"`
	Squad   string        `json:"squad"`
}

//Coordinates indicate an X and Y position on the game board
type Coordinates struct {
	X int `json:"x"`
	Y int `json:"y"`
}

//GameRequest is the message that gets posted to `/start` and `/move` when a new game is started or it is my turn to move
type BattleSnakeGameRequest struct {
	Game  BattleSnakeGame  `json:"game"`
	Turn  int              `json:"turn"`
	Board BattleSnakeBoard `json:"board"`
	Me    BattleSnakeSnake `json:"you"`
}

//BattleSnakeMoveResponse represents the json that needs to be sent back when they /move endpoint is hit.
type BattleSnakeMoveResponse struct {
	Move  Direction `json:"move"`
	Shout string    `json:"shout"`
}

//Direction represents one of the 4 valid directions a snake can move
type Direction string
