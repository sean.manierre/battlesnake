package cli

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/sean.manierre/battlesnake/pkg/game"
)

var logCmd = &cobra.Command{
	Use:   "logging",
	Short: "Used to enable or disable logging games out to json files.",
}

var enableLoggingCmd = &cobra.Command{
	Use:   "enable",
	Short: "Enables server logging of json files from games played.",
	Run: func(cmd *cobra.Command, args []string) {
		res, err := sendLoggingRequest(ENABLE)
		if err != nil {
			log.Fatalf("error when sending logging enable request: %s", err.Error())
		}
		log.Printf("%+v\n", res)
	},
}

var disableLoggingCmd = &cobra.Command{
	Use:   "disable",
	Short: "Disables server logging of json files from games played.",
	Run: func(cmd *cobra.Command, args []string) {
		res, err := sendLoggingRequest(DISABLE)
		if err != nil {
			log.Fatalf("error when sending logging disable request: %s", err.Error())
		}
		log.Printf("%+v\n", res)
	},
}

var statusLoggingCmd = &cobra.Command{
	Use:   "status",
	Short: "Checks to see whether or not the server is currently logging games",
	Run: func(cmd *cobra.Command, args []string) {
		res, err := sendLoggingRequest(STATUS)
		if err != nil {
			log.Fatalf("error when sending logging status request: %s", err.Error())
		}
		fmt.Printf("Logging status: %s\n", res.Data)
	},
}

var collectLogsCmd = &cobra.Command{
	Use:   "collect",
	Short: "Used to collect all the game logs on the remote system and download them to the current system",
	Long:  "A target directory can be specified to store all the game files. If this directory is not specified, the files are stored in the current directory.",
	Args:  cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		//The only argument will be a filepath to store the downloaded game files.
		if len(args) == 1 {
			if _, err := os.Stat(args[0]); os.IsNotExist(err) {
				// Path to store doesn't exist, create it
				os.Mkdir(args[0], 0777)
			}
			res, err := sendLoggingRequest(COLLECT)
			if err != nil {
				log.Fatalf("error when sending logging collection request: %s", err.Error())
			}
			var games struct {
				Games []game.GameRecord `json:"games"`
			}
			jsonString := strings.NewReader(res.Data)
			json.NewDecoder(jsonString).Decode(&games)
			for _, game := range games.Games {
				f, err := os.Create(fmt.Sprintf("%s/%s.json", args[0], game.GameId))
				if err != nil {
					log.Printf("Error writing out game to file: %s\n", err.Error())
				}
				defer f.Close()
				json.NewEncoder(f).Encode(game)
			}
		}
	},
}

func sendLoggingRequest(reqType string) (CliResponse, error) {
	loggingRequest := CliRequest{
		Action:  LOGGING,
		Payload: reqType,
	}
	res, err := sendRequest(loggingRequest)
	if err != nil {
		return CliResponse{}, fmt.Errorf("error return from logging request: %s", err.Error())
	}
	return res, nil
}
