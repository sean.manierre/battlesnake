package cli

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	//TODO: comment this out before shipping
	rootURL = "https://gosnek.seanmanierre.tech"
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	//Main commands
	rootCmd.AddCommand(serverCmd)
	rootCmd.AddCommand(logCmd)

	//Logging commands
	logCmd.AddCommand(enableLoggingCmd)
	logCmd.AddCommand(disableLoggingCmd)
	logCmd.AddCommand(statusLoggingCmd)
	logCmd.AddCommand(collectLogsCmd)
	//Server commands
}

var rootCmd = &cobra.Command{
	Use: "bscli",
}

//TODO: Add a config option to set values?
//This is the root URL to use for the CLI when reaching out to the server.
var rootURL string

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}
}

func sendRequest(req CliRequest) (CliResponse, error) {
	b, err := json.Marshal(req)
	if err != nil {
		return CliResponse{}, fmt.Errorf("error when marshalling request to json: %s", err.Error())
	}
	reqReader := bytes.NewReader(b)
	res, err := http.Post(fmt.Sprintf("%s/cli", rootURL), "application/json", reqReader)
	if err != nil {
		return CliResponse{}, fmt.Errorf("error when sending request: %s", err.Error())
	}
	if res.StatusCode != http.StatusOK {
		return CliResponse{}, fmt.Errorf("error when attempting to disable logging: %s", res.Status)
	}
	var cliRes CliResponse
	json.NewDecoder(res.Body).Decode(&cliRes)
	defer res.Body.Close()
	return cliRes, nil
}
