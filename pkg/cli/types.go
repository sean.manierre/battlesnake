package cli

//CliRequest represents a request from the CLI to the server
type CliRequest struct {
	Action  string `json:"action"`
	Payload string `json:"payload"`
}

type CliResponse struct {
	Action string `json:"action"`
	Data   string `json:"data"`
}

//Constants to prevent typos causing errors. They should all be pretty self explanatory within the CLI
const (
	LOGGING = "LOGGING"
	ENABLE  = "ENABLE"
	DISABLE = "DISABLE"
	STATUS  = "STATUS"
	COLLECT = "COLLECT"
)
