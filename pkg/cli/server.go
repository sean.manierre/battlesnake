package cli

import "github.com/spf13/cobra"

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Used to interact with the battlesnake webserver",
	Long:  "Can be used to update certain aspects of the webserver without restarting it.",
}
