package game

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"testing"

	"gitlab.com/sean.manierre/battlesnake/api"
)

func TestExport(t *testing.T) {
	workingDir, err := os.Getwd()
	if err != nil {
		t.Errorf("Unable to setup properly, cannot get working dir.")
	}
	fp := fmt.Sprintf("%s/%s", workingDir, "games")
	t.Cleanup(func() {
		err := os.RemoveAll(fp)
		if err != nil {
			t.Errorf("Unable to cleanup test directory: %s", err.Error())
		}
	})
	var request api.BattleSnakeGameRequest
	json.NewDecoder(strings.NewReader(validJson)).Decode(&request)
	r := NewGameRecord("test", "none", request)
	err = r.Export()
	if err != nil {
		t.Errorf("Error when exporting gamefile: %s", err.Error())
	}
	if _, err := os.Stat(fp); os.IsNotExist(err) {
		t.Errorf("Expected directory '%s'. Directory not found", fp)
		fmt.Println(err == nil)
	}
	if _, err := os.Stat(fmt.Sprintf("%s/test.json", fp)); os.IsNotExist(err) {
		t.Errorf("Expected file '%s/test.json' to exist.", fp)
	}
}

const validJson = `{"game":{"id":"game-00fe20da-94ad-11ea-bb37","ruleset":{"name":"standard","version":"v.1.2.3"},"timeout":500},"turn":14,"board":{"height":11,"width":11,"food":[{"x":5,"y":5},{"x":9,"y":0},{"x":2,"y":6}],"hazards":[{"x":3,"y":2}],"snakes":[{"id":"snake-508e96ac-94ad-11ea-bb37","name":"My Snake","health":54,"body":[{"x":0,"y":0},{"x":1,"y":0},{"x":2,"y":0}],"latency":"111","head":{"x":0,"y":0},"length":3,"shout":"why are we shouting??","squad":""}]},"you":{"id":"snake-508e96ac-94ad-11ea-bb37","name":"My Snake","health":54,"body":[{"x":0,"y":0},{"x":1,"y":0},{"x":2,"y":0}],"latency":"111","head":{"x":0,"y":0},"length":3,"shout":"why are we shouting??","squad":""}}`
