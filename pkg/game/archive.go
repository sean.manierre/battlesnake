package game

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/sean.manierre/battlesnake/api"
)

//GameRecord is all the moves in a game that can be written out of the program to be used later for replays and analysis.
type GameRecord struct {
	GameId       string                     `json:"game_id"`
	Turns        map[int]*TurnInfo          `json:"turns"`
	Strategy     string                     `json:"strategy"`
	StartRequest api.BattleSnakeGameRequest `json:"start"`
	EndRequest   api.BattleSnakeGameRequest `json:"end"`
}

//TurnInfo contains the information about the state of the board and the move that was made in response.
type TurnInfo struct {
	GameState api.BattleSnakeGameRequest `json:"game_state"`
	Response  api.Direction              `json:"response"`
}

func NewGameRecord(id string, strategy string, request api.BattleSnakeGameRequest) *GameRecord {
	return &GameRecord{
		GameId:   id,
		Turns:    map[int]*TurnInfo{},
		Strategy: strategy,
	}
}

func (g *GameRecord) Export() error {
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("unable to get working directory: %s", err.Error())
	}
	fp := fmt.Sprintf("%s/games", wd)
	if _, err := os.Stat(fp); os.IsNotExist(err) {
		err := os.Mkdir(fp, 0744)
		if err != nil {
			return fmt.Errorf("unable to create directory 'games': %s", err.Error())
		}
	}
	outfile, err := os.Create(fmt.Sprintf("%s/%s.json", fp, g.GameId))
	if err != nil {
		return fmt.Errorf("unable to create file %s: %s", fmt.Sprintf("%s/%s.json", fp, g.GameId), err.Error())
	}
	err = json.NewEncoder(outfile).Encode(g)
	if err != nil {
		return fmt.Errorf("unable to encode json to gamefile: %s", err.Error())
	}
	return nil
}
