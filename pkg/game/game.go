package game

import (
	"log"

	"gitlab.com/sean.manierre/battlesnake/api"
)

//Game is an ongoing game of battlesnake. All of the game state will be contained in here.
type Game struct {
	Turn    int
	Board   api.BattleSnakeBoard
	MySnake api.BattleSnakeSnake
	Facing  api.Direction
	record  *GameRecord
}

//New returns a new game with all of the info parsed from the request.
func New(request api.BattleSnakeGameRequest) *Game {
	return &Game{
		Turn:    request.Turn,
		Board:   request.Board,
		MySnake: request.Me,
		record:  NewGameRecord(request.Game.Id, "none", request), //TODO: Update this hardcoded strategy whenever they are implemented.
		Facing:  api.UP,
	}
}

//GetNextMove uses chained if statements, I mean AI, to get the next move.
func (g *Game) GetNextMove() api.Direction {
	move := g.getNextMove()
	g.record.Turns[g.Turn].Response = move
	g.Facing = move
	return move
}

//Going to have all the logic in this function so I don't need to worry about
//capturing the move if I have multiple spots where the move is returned.
func (g *Game) getNextMove() api.Direction {
	// if g.wallAhead() {
	// 	return g.clockwise()
	// }
	// return g.Facing
	log.Printf("Turn: %d\n", g.Turn)
	nearestFoodDirection := g.findFood()
	for g.willCollideWithSelf(nearestFoodDirection) || g.wallAhead(nearestFoodDirection) {
		log.Printf("Attempting to avoid collision by going: %s\n", g.clockwise(nearestFoodDirection))
		nearestFoodDirection = g.clockwise(nearestFoodDirection)
	}
	return nearestFoodDirection
}

//Update takes in the info from the newest request and updates the game state with it.
func (g *Game) Update(request api.BattleSnakeGameRequest) {
	g.Board = request.Board
	g.MySnake = request.Me
	g.Turn = request.Turn
	if _, ok := g.record.Turns[request.Turn]; ok {
		g.record.Turns[request.Turn].GameState = request
	} else {
		g.record.Turns[request.Turn] = &TurnInfo{GameState: request}
	}
}

func (g *Game) End(request api.BattleSnakeGameRequest) {
	g.record.EndRequest = request
	if err := g.record.Export(); err != nil {
		log.Printf("Unable to write out game file: %s\n", err.Error())
	}
}

func (g *Game) wallAhead(direction api.Direction) bool {
	switch direction {
	case api.LEFT:
		if g.MySnake.Head.X == 0 {
			return true
		}
	case api.UP:
		if g.MySnake.Head.Y == g.Board.Height-1 {
			return true
		}
	case api.RIGHT:
		if g.MySnake.Head.X == g.Board.Width-1 {
			return true
		}
	case api.DOWN:
		if g.MySnake.Head.Y == 0 {
			return true
		}
	}
	return false
}

func (g *Game) clockwise(dir api.Direction) api.Direction {
	switch dir {
	case api.UP:
		return api.RIGHT
	case api.RIGHT:
		return api.DOWN
	case api.DOWN:
		return api.LEFT
	case api.LEFT:
		return api.UP
	}
	return g.Facing
}

//findFood returns the direction to get to the closest food
func (g *Game) findFood() api.Direction {
	foodCoordinates := g.Board.Food
	var totalMovesAway, totalHorizontal, totalVertical int
	for i, coord := range foodCoordinates {
		horizontalDistance := coord.X - g.MySnake.Head.X
		verticalDistance := coord.Y - g.MySnake.Head.Y
		if i == 0 {
			totalMovesAway = intABS(horizontalDistance) + intABS(verticalDistance)
			totalHorizontal = horizontalDistance
			totalVertical = verticalDistance

			continue
		}
		if (intABS(horizontalDistance) + intABS(verticalDistance)) < totalMovesAway {
			totalMovesAway = intABS(horizontalDistance) + intABS(verticalDistance)
			totalHorizontal = horizontalDistance
			totalVertical = verticalDistance
		}
	}
	log.Printf("Total Vertical: %d\n", totalVertical)
	log.Printf("Total Horizontal: %d\n", totalHorizontal)
	if (totalVertical > totalHorizontal || totalVertical == 0) && totalHorizontal != 0 {
		log.Println("Horizontal distance is shorter, or vertical is 0")
		if totalHorizontal > 0 {
			log.Println("Moving right")
			return api.RIGHT
		} else {
			log.Println("Moving left")
			return api.LEFT
		}
	} else {
		log.Println("Vertical distance is shorter")
		if totalVertical > 0 {
			log.Println("Moving up")
			return api.UP
		} else {
			log.Println("Moving down")
			return api.DOWN
		}
	}
}

//Provided a direction, returns whether or not the snake is going to collide with itself.
func (g *Game) willCollideWithSelf(direction api.Direction) bool {
	var nextCoord api.Coordinates
	log.Printf("Checking for Collisions for direction: %s\n", direction)
	switch direction {
	case api.UP:
		nextCoord = api.Coordinates{Y: g.MySnake.Head.Y + 1, X: g.MySnake.Head.X}
	case api.DOWN:
		nextCoord = api.Coordinates{Y: g.MySnake.Head.Y - 1, X: g.MySnake.Head.X}
	case api.LEFT:
		nextCoord = api.Coordinates{X: g.MySnake.Head.X - 1, Y: g.MySnake.Head.Y}
	case api.RIGHT:
		nextCoord = api.Coordinates{X: g.MySnake.Head.X + 1, Y: g.MySnake.Head.Y}
	}
	log.Printf("Current Head Position: %+v\n", g.MySnake.Head)
	log.Printf("Checking coordinate: %+v\n", nextCoord)
	for _, bodyPart := range g.MySnake.Body {
		log.Printf("Checking for collision with: %+v\n", bodyPart)
		if nextCoord == bodyPart {
			log.Printf("Found collision with %+v\n", bodyPart)
			return true
		}
	}
	return false
}

//Since apparently there is no built in int abs func
func intABS(n int) int {
	if n < 0 {
		return -n
	}
	return n
}
