package game

import (
	"testing"

	"gitlab.com/sean.manierre/battlesnake/api"
)

func TestWallAhead(t *testing.T) {

}

func TestClockWise(t *testing.T) {
	g := New(api.BattleSnakeGameRequest{})
	g.Facing = api.UP
	nextDir := g.clockwise(g.Facing)
	if nextDir != api.RIGHT {
		t.Errorf("Expected %s, got %s", api.RIGHT, nextDir)
	}
	g.Facing = nextDir
	nextDir = g.clockwise(g.Facing)
	if nextDir != api.DOWN {
		t.Errorf("Expected %s, got %s", api.DOWN, nextDir)
	}
	g.Facing = nextDir
	nextDir = g.clockwise(g.Facing)
	if nextDir != api.LEFT {
		t.Errorf("Expected %s, got %s", api.LEFT, nextDir)
	}
	g.Facing = nextDir
	nextDir = g.clockwise(g.Facing)
	if nextDir != api.UP {
		t.Errorf("Expected %s, got %s", api.UP, nextDir)
	}
}
