package shout

import "math/rand"

func Shout() string {
	return quotes[rand.Int()%len(quotes)]
}

var quotes = []string{
	"Tis but a scratch",
	"What are you gonna do, bleed on me?",
	"I fart in your general direction",
	"Help! Help! I'm being repressed!",
	"I've soiled my armor!",
	"Nobody expects the spanish inquisition!",
	"What're you gonna do? Eat me?",
}
