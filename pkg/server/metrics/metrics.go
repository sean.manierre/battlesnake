package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var CliRequestNumberMetric = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "cli_request_count",
		Help: "The total number of cli requests received",
	},
)

var GameStartNumberMetric = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "game_start_count",
		Help: "Total number of games started",
	},
)

var GameTurnsHandledMetric = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "game_total_turn_count",
		Help: "Total number of moves since the server has been restarted",
	},
)

var TurnsPerGameMetric = promauto.NewHistogram(
	prometheus.HistogramOpts{
		Name:    "game_turns_per_game",
		Help:    "Average number of turns per game",
		Buckets: []float64{0, 25, 50, 100, 250, 500, 1000},
	},
)

var GameEndNumberMetric = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "game_end_count",
		Help: "Total number of games ended",
	},
)
