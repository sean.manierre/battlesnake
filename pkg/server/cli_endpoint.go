package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/sean.manierre/battlesnake/pkg/cli"
	"gitlab.com/sean.manierre/battlesnake/pkg/game"
	"gitlab.com/sean.manierre/battlesnake/pkg/server/metrics"
)

func (a *ApiServer) handleCliCall(w http.ResponseWriter, r *http.Request) {
	//TODO: Figure out a way to authorize cli calls once it's working.
	// if r.Header.Get("Authorization") == "" {
	// 	w.WriteHeader(http.StatusForbidden)
	// 	return
	// }
	metrics.CliRequestNumberMetric.Inc()
	var cliReq cli.CliRequest
	json.NewDecoder(r.Body).Decode(&cliReq)
	defer r.Body.Close()
	switch cliReq.Action {
	case cli.LOGGING:
		a.handleLogging(w, cliReq)
	}
}

func (a *ApiServer) handleLogging(w http.ResponseWriter, req cli.CliRequest) {
	log.Printf("Handling logging %s request\n", req.Payload)
	cliResponse := cli.CliResponse{}
	cliResponse.Action = req.Action
	switch req.Payload {
	case cli.STATUS:
		if a.loggingEnabled {
			cliResponse.Data = "Enabled"
		} else {
			cliResponse.Data = "Disabled"
		}
	case cli.ENABLE:
		a.loggingEnabled = true
		cliResponse.Data = ""
	case cli.DISABLE:
		a.loggingEnabled = false
		cliResponse.Data = ""
	case cli.COLLECT:
		gamesDir, err := os.ReadDir("games")
		if err != nil {
			log.Printf("Error when attempting to read games dir: %s\n", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		var games struct {
			Games []game.GameRecord `json:"games"`
		}
		var g game.GameRecord
		for _, f := range gamesDir {
			file, err := os.Open(fmt.Sprintf("games/%s", f.Name()))
			if err != nil {
				log.Printf("Error when attempting to open file \"%s\": %s\n", f.Name(), err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			err = json.NewDecoder(file).Decode(&g)
			if err != nil {
				log.Printf("Error when attempting to decode file \"%s\": %s\n", f.Name(), err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			file.Close()
			games.Games = append(games.Games, g)
		}
		jsonString, err := json.Marshal(games)
		if err != nil {
			log.Printf("Error when attempting to marshal slice of games to string: %s\n", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		cliResponse.Data = string(jsonString)
	default:
		log.Printf("CLI Action: %s not recognized\n", req.Action)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(cliResponse)
}
