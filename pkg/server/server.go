package server

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/sean.manierre/battlesnake/api"
	"gitlab.com/sean.manierre/battlesnake/pkg/game"
	"gitlab.com/sean.manierre/battlesnake/pkg/server/metrics"
	"gitlab.com/sean.manierre/battlesnake/pkg/shout"
)

var snakeInfo = api.BattleSnakeInfoResponse{
	APIVersion: "1",
	Author:     "TickleMeElmo",
	Color:      "#ffa31a",
	Head:       "snowman",
	Tail:       "small-rattle",
}

var currentGame *game.Game

//ApiServer uses `http.DefaultServeMux` under the hood to serve requests to the endpoints needed by the BattleSnake website.
type ApiServer struct {
	loggingEnabled bool
}

//ServeHTTP passes off any requests to `http.DefaultServeMux` that has all the routes registered with it.
func (a ApiServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.DefaultServeMux.ServeHTTP(w, r)
}

//New returns a new `ApiServer` and registers all the endpoints.
func New() ApiServer {
	a := ApiServer{
		loggingEnabled: false,
	}
	http.Handle("/", http.HandlerFunc(handleRoot))
	http.Handle("/start", http.HandlerFunc(handleStart))
	http.Handle("/move", http.HandlerFunc(handleMove))
	http.Handle("/end", http.HandlerFunc(handleEnd))
	http.Handle("/cli", http.HandlerFunc(a.handleCliCall))
	http.Handle("/metrics", promhttp.Handler())
	return a
}

//`/` is requested everyonce in a while to check on the health of the server. The only response needed
//is the SnakeInfo struct above.
func handleRoot(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(snakeInfo)
	if err != nil {
		log.Printf("Error when encoding Snake Info: %s\n", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}
}

//`/start` is requested at the beginning of the game. The info provided will be used to make a new `game.Game`.
//Not sure how i'm going to implement the game logic yet, but will kick it off from here.
func handleStart(w http.ResponseWriter, r *http.Request) {
	metrics.GameStartNumberMetric.Inc()
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	//Handle request
	var gameRequest api.BattleSnakeGameRequest
	err := json.NewDecoder(r.Body).Decode(&gameRequest)
	defer r.Body.Close()
	if err != nil {
		log.Printf("Error when decoding Game Request: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	currentGame = game.New(gameRequest)

	//Handle response
	w.WriteHeader(http.StatusOK)
}

//`/move` is requested whenever it is my turn to make a move. Will forwards the received info into the game engine to determine
//the next move.
func handleMove(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	//Handle request
	var gameRequest api.BattleSnakeGameRequest
	err := json.NewDecoder(r.Body).Decode(&gameRequest)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error when decoding game request: %s\n", err.Error())
		return
	}
	currentGame.Update(gameRequest)
	//Handle response
	w.Header().Set("Content-Type", "application/json")
	var move api.BattleSnakeMoveResponse
	move.Shout = shout.Shout()
	move.Move = currentGame.GetNextMove()
	err = json.NewEncoder(w).Encode(move)
	if err != nil {
		log.Printf("Error when encoding move response: %s\n", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}
}

//`/end` is requested when the game is over. Can use this to do any post game analysis and make sure theres nothing still running
//that doesn't need to be.
func handleEnd(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	//Handle request
	var gameRequest api.BattleSnakeGameRequest
	err := json.NewDecoder(r.Body).Decode(&gameRequest)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error when decoding game request: %s\n", err.Error())
		return
	}
	metrics.GameTurnsHandledMetric.Add(float64(gameRequest.Turn))
	metrics.TurnsPerGameMetric.Observe(float64(gameRequest.Turn))
	metrics.GameEndNumberMetric.Inc()
	currentGame.End(gameRequest)
	//Handle response
	w.WriteHeader(http.StatusOK)
}
