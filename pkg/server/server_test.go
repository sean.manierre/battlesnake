package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/sean.manierre/battlesnake/api"
)

//Cleans up after the tests run so the next test doesn't try to re-register existing routes.
//Can't run in t.Cleanup because that would clear the routes after the first test
func resetServer() {
	http.DefaultServeMux = http.NewServeMux()
}

func TestRoot(t *testing.T) {
	resetServer()
	s := New()
	tc := []struct {
		Name                 string
		Method               string
		ExpectedResponseCode int
		ExpectedResponseBody api.BattleSnakeInfoResponse
	}{
		{
			Name:                 "Get method",
			Method:               http.MethodGet,
			ExpectedResponseCode: http.StatusOK,
			ExpectedResponseBody: snakeInfo,
		},
		{
			Name:                 "Post method",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			ExpectedResponseBody: api.BattleSnakeInfoResponse{},
		},
		{
			Name:                 "Put method",
			Method:               http.MethodPut,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			ExpectedResponseBody: api.BattleSnakeInfoResponse{},
		},
		{
			Name:                 "Delete method",
			Method:               http.MethodDelete,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			ExpectedResponseBody: api.BattleSnakeInfoResponse{},
		},
	}
	for _, tt := range tc {
		t.Run(tt.Name, func(t *testing.T) {
			req := httptest.NewRequest(tt.Method, "/", nil)
			res := httptest.NewRecorder()
			s.ServeHTTP(res, req)
			if res.Result().StatusCode != tt.ExpectedResponseCode {
				t.Errorf("Expected status code %d, got %d\n", tt.ExpectedResponseCode, res.Result().StatusCode)
			}
			resData := api.BattleSnakeInfoResponse{}
			json.NewDecoder(res.Body).Decode(&resData)
			if resData != tt.ExpectedResponseBody {
				t.Errorf("Expected response body %+v, got %+v\n", tt.ExpectedResponseBody, resData)
			}
		})
	}
	http.DefaultServeMux = http.NewServeMux()
}

func TestStart(t *testing.T) {
	resetServer()
	s := New()
	tc := []struct {
		Name                 string
		Method               string
		ExpectedResponseCode int
		RequestBody          string //this should be the body of the request in json format.
	}{
		{
			Name:                 "Get method",
			Method:               http.MethodGet,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
		{
			Name:                 "Valid post",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusOK,
			RequestBody:          validJson,
		},
		{
			Name:                 "Invalid post",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusBadRequest,
			RequestBody:          "",
		},
		{
			Name:                 "Put method",
			Method:               http.MethodPut,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          validJson,
		},
		{
			Name:                 "Delete method",
			Method:               http.MethodDelete,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
	}
	for _, tt := range tc {
		t.Run(tt.Name, func(t *testing.T) {
			body := bytes.NewBuffer([]byte(tt.RequestBody))
			req := httptest.NewRequest(tt.Method, "/start", body)
			res := httptest.NewRecorder()
			s.ServeHTTP(res, req)
			if res.Result().StatusCode != tt.ExpectedResponseCode {
				t.Errorf("Expected status code %d, got %d\n", tt.ExpectedResponseCode, res.Result().StatusCode)
			}
		})
	}
}

func TestMove(t *testing.T) {
	resetServer()
	s := New()
	tc := []struct {
		Name                 string
		Method               string
		ExpectedResponseCode int
		RequestBody          string
	}{
		{
			Name:                 "Get method",
			Method:               http.MethodGet,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
		{
			Name:                 "Valid post",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusOK,
			RequestBody:          validJson,
		},
		{
			Name:                 "Invalid post",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusBadRequest,
			RequestBody:          "",
		},
		{
			Name:                 "Put method",
			Method:               http.MethodPut,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
		{
			Name:                 "Delete method",
			Method:               http.MethodPut,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
	}

	for _, tt := range tc {
		t.Run(tt.Name, func(t *testing.T) {
			body := bytes.NewBuffer([]byte(tt.RequestBody))
			req := httptest.NewRequest(tt.Method, "/move", body)
			res := httptest.NewRecorder()
			s.ServeHTTP(res, req)
			if res.Result().StatusCode != tt.ExpectedResponseCode {
				t.Errorf("Expected status code: %d, got: %d\n", tt.ExpectedResponseCode, res.Result().StatusCode)
			}
			//This test is supposed to pass, make sure the response is valid json.
			if tt.Method == http.MethodPost && tt.ExpectedResponseCode == http.StatusOK {
				jsonRes := res.Body.String()
				var moveRes api.BattleSnakeMoveResponse
				err := json.NewDecoder(strings.NewReader(jsonRes)).Decode(&moveRes)
				if err != nil {
					t.Errorf("Invalid json response: %s\n", jsonRes)
				}
				if moveRes.Move == "" || moveRes.Shout == "" {
					t.Errorf("Invalid game.BattleSnakeMoveResponse: %+v\n", moveRes)
				}
			}
		})
	}
}

func TestEnd(t *testing.T) {
	resetServer()
	s := New()
	tc := []struct {
		Name                 string
		Method               string
		ExpectedResponseCode int
		RequestBody          string
	}{
		{
			Name:                 "Get method",
			Method:               http.MethodGet,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
		{
			Name:                 "Valid post",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusOK,
			RequestBody:          validJson,
		},
		{
			Name:                 "Invalid post",
			Method:               http.MethodPost,
			ExpectedResponseCode: http.StatusBadRequest,
			RequestBody:          "",
		},
		{
			Name:                 "Put method",
			Method:               http.MethodPut,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
		{
			Name:                 "Delete method",
			Method:               http.MethodDelete,
			ExpectedResponseCode: http.StatusMethodNotAllowed,
			RequestBody:          "",
		},
	}
	for _, tt := range tc {
		t.Run(tt.Name, func(t *testing.T) {
			body := bytes.NewBuffer([]byte(tt.RequestBody))
			req := httptest.NewRequest(tt.Method, "/end", body)
			res := httptest.NewRecorder()
			s.ServeHTTP(res, req)
			if res.Result().StatusCode != tt.ExpectedResponseCode {
				t.Errorf("Expected status code: %d, got: %d\n", tt.ExpectedResponseCode, res.Result().StatusCode)
			}
		})
	}
	workingDir, err := os.Getwd()
	if err != nil {
		t.Errorf("Unable to setup properly, cannot get working dir.")
	}
	fp := fmt.Sprintf("%s/%s", workingDir, "games")
	t.Cleanup(func() {
		err := os.RemoveAll(fp)
		if err != nil {
			t.Errorf("Unable to cleanup test directory: %s", err.Error())
		}
	})
}

const validJson = `{"game":{"id":"game-00fe20da-94ad-11ea-bb37","ruleset":{"name":"standard","version":"v.1.2.3"},"timeout":500},"turn":14,"board":{"height":11,"width":11,"food":[{"x":5,"y":5},{"x":9,"y":0},{"x":2,"y":6}],"hazards":[{"x":3,"y":2}],"snakes":[{"id":"snake-508e96ac-94ad-11ea-bb37","name":"My Snake","health":54,"body":[{"x":0,"y":0},{"x":1,"y":0},{"x":2,"y":0}],"latency":"111","head":{"x":0,"y":0},"length":3,"shout":"why are we shouting??","squad":""}]},"you":{"id":"snake-508e96ac-94ad-11ea-bb37","name":"My Snake","health":54,"body":[{"x":0,"y":0},{"x":1,"y":0},{"x":2,"y":0}],"latency":"111","head":{"x":0,"y":0},"length":3,"shout":"why are we shouting??","squad":""}}`
