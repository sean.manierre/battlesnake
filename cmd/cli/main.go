package main

import "gitlab.com/sean.manierre/battlesnake/pkg/cli"

func main() {
	cli.Execute()
}
