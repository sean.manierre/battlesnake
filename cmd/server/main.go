package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/sean.manierre/battlesnake/pkg/server"
)

func main() {
	tls := flag.Bool("tls", false, "Pass this flag to use TLS. If so must specify locations for certificate and key files also.")
	certFile := flag.String("certfile", "", "The path to the certificate file to use for TLS.")
	keyFile := flag.String("keyfile", "", "The path to the key file to use for TLS.")
	port := flag.String("port", "8080", "The port the server should listen on. Defaults to 8080")
	flag.Parse()

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err.Error())
	}
	fp := fmt.Sprintf("%s/logs", wd)
	os.Mkdir(fp, 0744)
	logfile, err := os.Create(fmt.Sprintf("%s/server.log", fp))
	if err != nil {
		panic(fmt.Errorf("unable to create log file: %s", err.Error()))
	}
	log.SetOutput(logfile)
	server := server.New()
	if !*tls {
		fmt.Printf("Listening on: %s:%s\n", "", *port)
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", *port), server))
	}
	log.Fatal(http.ListenAndServeTLS(fmt.Sprintf(":%s", *port), *certFile, *keyFile, server))
}
