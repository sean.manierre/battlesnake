#!/bin/sh

# Install supervisor and setup to look in the /etc/supervisord.d directory for conf files.
sudo amazon-linux-extras install -y epel
sudo yum install -y supervisor-3.4.0
echo "files = /etc/supervisord.d/*.conf" | sudo tee -a /etc/supervisord.conf > /dev/null

#Copy battlesnake.conf to /etc/supervisord.d so it gets loaded by supervisor
sudo mv battlesnake.conf /etc/supervisord.d/battlesnake.conf

#Start supervisord
sudo supervisord -c /etc/supervisord.conf

# Generate self-signed certificates to get TLS with cloudflare
openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 365 \
            -nodes \
            -out cert.cer \
            -keyout key.key \
            -subj "/C=US/ST=CT/L=City/O=Battlesnake/OU=Developer/CN=gosnek.seanmanierre.tech"
