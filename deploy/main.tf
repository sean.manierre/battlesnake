terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.64.2"
    }
  }
  backend "remote" {
    organization = "seanmanierre"

    workspaces {
      name = "battlesnake"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow https traffic in, and ssh from my IP."
  vpc_id      = data.aws_vpc.default.id

  ingress = [
    {
      description      = "TLS from anywhere"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      security_groups  = []
      prefix_list_ids  = []
      self             = false
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    },
    {
      description      = "SSH from my IP and Terraform Cloud"
      from_port        = 22
      to_port          = 22
      security_groups  = []
      prefix_list_ids  = []
      self             = false
      protocol         = "tcp"
      cidr_blocks      = [var.MY_IP]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  egress = [
    {
      description      = "Send traffic to anywhere"
      security_groups  = []
      prefix_list_ids  = []
      self             = false
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  tags = {
    Name = "allow_tls and ssh"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = var.AWS_DEPLOYER_PUBLIC_KEY
}

resource "aws_instance" "battlesnake" {
  ami             = "ami-01cc34ab2709337aa"
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.deployer.key_name
  security_groups = [aws_security_group.allow_tls.name]
}

resource "null_resource" "configure" {
  provisioner "local-exec" {
    command = "aws configure set default.region us-east-1"
  }
}

resource "null_resource" "status" {
  provisioner "local-exec" {
    command = "aws ec2 wait instance-status-ok --instance-ids ${aws_instance.battlesnake.id}"
  }

  depends_on = [
    aws_instance.battlesnake,
    null_resource.configure
  ]
}

output "public_ip" {
  value = aws_instance.battlesnake.public_ip
}
