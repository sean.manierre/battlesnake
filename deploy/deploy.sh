#!/bin/bash
source .env .

terraform apply | tee tf.log

if [[ ${PIPESTATUS[0]} -ne 0 ]]; then 
    echo "Non-zero exit code from terraform, please check output"
    exit 1
fi
rm tf.log

SERVER_IP=$(terraform output -raw public_ip)

echo "Building binaries..."
bash ../build/build.sh bscli server

echo "Copying server binary to server..."
scp -o StrictHostKeyChecking=no temp/server ec2-user@${SERVER_IP}:
rm -r temp

echo "Copying setup script to server..."
scp service_setup.sh ec2-user@${SERVER_IP}:

echo "Copying service configuration to server..."
scp battlesnake.conf ec2-user@${SERVER_IP}:

echo "Running service setup script..."
ssh ec2-user@${SERVER_IP} "bash service_setup.sh"

echo "Updating Cloudflare DNS..."
curl -X PUT "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/dns_records/${CLOUDFLARE_RECORD_ID}"\
        -H "X-Auth-Email: ${CLOUDFLARE_EMAIL}"\
        -H "X-Auth-Key: ${CLOUDFLARE_API_KEY}"\
        -H "Content-Type: application/json"\
        --data "{\"type\":\"A\", \"name\":\"${DOMAIN}\", \"content\":\"${SERVER_IP}\", \"ttl\":\"1\", \"proxied\":true}"