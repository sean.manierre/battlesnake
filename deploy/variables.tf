variable "MY_IP" {
  type = string
}

variable "AWS_DEPLOYER_PUBLIC_KEY" {
  type = string
}

variable "AWS_ACCESS_KEY_ID" {
  type = string
}

variable "AWS_SECRET_ACCESS_KEY" {
  type = string
}

variable "AWS_PRIVATE_KEY" {
  type = string
}
