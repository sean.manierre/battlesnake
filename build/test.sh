#!/bin/bash

# Run all the tests, if FAIL is found, return
if go test -v ./pkg/** | tee results.txt | grep -c 'FAIL' > /dev/null
then
    echo "Tests failed"
    echo "Results can be found in the results.txt artifact in addition to below"
    cat results.txt
    exit 1
elif [[ ${PIPESTATUS[0]} -eq 0 ]]
then 
    # If all the tests pass, get rid of the results file as it's no longer needed
    rm results.txt
    echo "All tests passed!"
    exit 0
fi
# If the return code from go test wasn't 0, but failed for some other reason still fail the pipeline
echo "Tests did not run correctly, see terminal output or results.txt for more information."
exit 1