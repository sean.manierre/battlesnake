#!/bin/bash

PROJECT_ROOT=$(cd ..; pwd)
# Assign all of the binary names to an array to be used later
COUNT=0 # Count to keep track of the index of the loop
NAMES=() # Array to hold the names stored
for arg in "$@"; do
  NAMES[COUNT]=$arg
  (( COUNT++ ))
done

# List the directories that have binaries to build
CMD_PATH="${PROJECT_ROOT}/cmd"
DIRS=$(ls $CMD_PATH)
DIR_COUNT=$(ls $CMD_PATH | wc -w)

# Check to see that the number of provided names matches the number of binaries to build
if [[ ${#NAMES[@]} -ne $DIR_COUNT ]]; then
  echo "Number of binaries to be built doesn't match the number of supplied names."
  echo "Please double check the arguments being passed to this script."
  echo "Supplied names: ${#NAMES[@]}"
  echo "Binaries to be built: ${DIR_COUNT}"
  exit 1
fi

# Reset COUNT back to 0
COUNT=0

# Loop through each binary to be built and build it with the provided name.
# Make sure that the names provided are correct for each binary being built.
# The binary name should be ${COMPONENT}_${TIMESTAMP}, where component should match the directory name.
for DIR in $DIRS; do
    if [[ $(echo ${NAMES[COUNT]} | grep -ic $DIR) -ne 1 ]]; then
        echo "Binary name supplied (${NAMES[COUNT]}) doesn't appear to match the binary to be built (${DIR})."
        echo "Please verify the order of the directories and the order of the names being passed into the script."
        exit 1
    fi
    go build -o $PROJECT_ROOT/deploy/temp/${NAMES[COUNT]} $CMD_PATH/$DIR
    (( COUNT++ ))
done